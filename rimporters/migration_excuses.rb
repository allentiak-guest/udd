#!/usr/bin/ruby

require 'yaml'
require 'time'

EXCUSES_URL = 'https://release.debian.org/britney/excuses.yaml'

def update_migration_excuses
  db = PG.connect(UDD_USER_PG)
  db.exec("BEGIN")
  db.exec("SET CONSTRAINTS ALL DEFERRED")
  db.exec("DELETE FROM migration_excuses")

  # all fields: ["missing-builds", "old-binaries"]
  db.prepare('me_insert', "INSERT INTO migration_excuses
      (item_name, source, migration_policy_verdict, old_version, new_version, is_candidate, excuses, reason, hints, policy_info, dependencies, invalidated_by_other_package, missing_builds, old_binaries)
      VALUES
      ($1, $2, $3, $4, $5, $6, $7::text[], $8::text[], $9::text[], $10, $11, $12, $13, $14)")

  excuses = YAML::load(open(EXCUSES_URL).read)
  if excuses['generated-date'] < Time::now - 86400
    raise "Excuses file is too old" 
  end
  excuses['sources'].each do |e|
    hints = e['hints'] ? e['hints'].map { |h| h.values.join(':') } : nil
    db.exec_prepared( 'me_insert', [
      e['item-name'],
      e['source'],
      e['migration-policy-verdict'],
      e['old-version'],
      e['new-version'],
      e['is-candidate'],
      PG::TextEncoder::Array.new.encode(e['excuses']),
      PG::TextEncoder::Array.new.encode(e['reason']),
      PG::TextEncoder::Array.new.encode(hints),
      JSON::dump(e['policy_info']),
      JSON::dump(e['dependencies']),
      e['invalidated-by-other-package'],
      JSON::dump(e['missing-builds']),
      JSON::dump(e['old-binaries'])
    ])
  end
  db.exec("COMMIT")
end
