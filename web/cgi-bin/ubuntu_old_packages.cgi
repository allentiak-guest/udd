#!/usr/bin/ruby
# Used by DDPO

$:.unshift('../../rlibs')
require 'udd-db'
require 'yaml'

RELEASE=YAML::load(IO::read('../ubuntu-releases.yaml'))['devel']
RELEASELTS=YAML::load(IO::read('../ubuntu-releases.yaml'))['lts']

puts "Content-type: text/html\n\n"

DB = Sequel.connect(UDD_GUEST)
popcon = DB["select insts from ubuntu_popcon_src where source='coreutils'"].all.hash_values
puts "Popcon for coreutils: #{popcon}<br>"
mpc = popcon/1000*100
puts "0.2% of coreutils popcon: #{mpc}<br>"

puts "### was in etch | lenny<br>"
DB["select src1.source, src1.version, coalesce(insts, 0) insts
from ubuntu_sources src1
join ubuntu_sources src2 using (source, version)
left join ubuntu_popcon_src popcon using (source)
where src1.component in ('universe', 'multiverse')
and src1.release='#{RELEASE}' and src2.release='#{RELEASELTS}'
and src1.source not in
  (select source from sources where release = 'sid')
and src1.source in
  (select source from sources where release in ('etch','lenny'))
and insts < #{mpc}
order by insts asc"].all.sym2str.each do |r|
  puts "<a href=\"https://packages.ubuntu.com/search?searchon=sourcenames&keywords=#{r['source']}\">#{r['source']}</a> #{r['version']} #{r['insts']} <a href=\"https://packages.qa.debian.org/#{r['source']}\">PTS</a><br>"
end

puts "### was NOT in etch | lenny<br>"
DB["select src1.source, src1.version, coalesce(insts, 0) insts
from ubuntu_sources src1
join ubuntu_sources src2 using (source, version)
left join ubuntu_popcon_src popcon using (source)
where src1.component in ('universe', 'multiverse')
and src1.release='#{RELEASE}' and src2.release='#{RELEASELTS}'
and src1.source not in
  (select source from sources where release = 'sid')
and src1.source not in
  (select source from sources where release in ('etch','lenny'))
and insts < #{mpc}
order by insts asc"].all.sym2str.each do |r|
  puts "<a href=\"https://packages.ubuntu.com/search?searchon=sourcenames&keywords=#{r['source']}\">#{r['source']}</a> #{r['version']} #{r['insts']} <a href=\"https://packages.qa.debian.org/#{r['source']}\">PTS</a><br>"
end
