#!/usr/bin/ruby
# Used by DDPO

$:.unshift('../../rlibs')
require 'udd-db'

puts "Content-type: text/plain\n\n"

DB = Sequel.connect(UDD_GUEST)

DB["select format, count(*) as cnt
from sources_uniq
where distribution='debian' and release='sid'
group by format order by format asc"].all.sym2str.each do |row|
  puts "#{row['cnt']} #{row['format']}"
end
